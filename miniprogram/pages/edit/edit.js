var util = require('../../utils/util.js');
var cryptoJs = require('../../utils/cryptoJs.js');
var keyService = require('../../data/keyService.js');
const globalData = getApp().globalData;

Page({
  data: {
    navBarTitleText: "添加钥匙",
    catArry: [],
    hModalVisible: false,
    keyModel: {
      categoryID: -1,
      categoryName: '',
      categoryColor: '',
      keyID: -1,
      keyName: '',
      keyAccount: '',
      keyPwd: '',
      keyPwd2: '',
      keyUrl: '',
      keyRemark: ''
    }
  },
  //弹出分类
  catOpenBind(event) {
    this.setData({
      hModalVisible: true
    });
  },
  //选中分类
  catClickBind(event) {
    const category = event.currentTarget.dataset.catitem;
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: category.color,
    });
    const keyModel = this.data.keyModel;
    keyModel.categoryID = category.id;
    keyModel.categoryName = category.name;
    keyModel.categoryColor = category.color;
    keyModel.categorySort = category.sort;

    this.setData({
      hModalVisible: false,
      keyModel: keyModel
    });
  },
  //输入框失去焦点
  inputInputBind: function(event) {
    const inputValue = event.detail.value;
    const inputName = event.currentTarget.dataset.name;

    this.data.keyModel[inputName] = inputValue;
  },
  //保存
  kaySaveBind: function(event) {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    const keyModel = util.copyObject(this.data.keyModel);
    if (keyModel.keyName == null || keyModel.keyName.trim() == "") {
      util.showToast('钥匙名称不允许为空!');
      return;
    }
    if (keyModel.keyAccount == null || keyModel.keyAccount.trim() == "") {
      util.showToast('账号不允许为空!');
      return;
    }

    //加密数据
    keyModel.categoryName = cryptoJs.AesEncrypt(keyModel.categoryName, globalData.password);

    keyModel.keyName = cryptoJs.AesEncrypt(keyModel.keyName, globalData.password);
    keyModel.keyAccount = cryptoJs.AesEncrypt(keyModel.keyAccount, globalData.password);
    keyModel.keyPwd = cryptoJs.AesEncrypt(keyModel.keyPwd, globalData.password);
    keyModel.keyPwd2 = cryptoJs.AesEncrypt(keyModel.keyPwd2, globalData.password);
    keyModel.keyUrl = cryptoJs.AesEncrypt(keyModel.keyUrl, globalData.password);
    keyModel.keyRemark = cryptoJs.AesEncrypt(keyModel.keyRemark, globalData.password);

    let localJsonData = util.getStorageData(globalData.localJsonName);
    keyService.editKey(keyModel, localJsonData);
    util.setStorageData(globalData.localJsonName, localJsonData);
    if (!globalData.userInfo.isCloudSyn) {
      util.navigateBack();
      return;
    }
    util.setCloudDbData(globalData.cloudJsonName, localJsonData).then(res => {
      util.navigateBack();
    }).catch(err => {
      console.error(err);

      util.showToast(this.data.navBarTitleText + '失败!');
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const localJsonData = util.getStorageData(globalData.localJsonName);
    const catArry = keyService.deCategoryArry(localJsonData.category, globalData.password); //解密数据

    let keyModel = {};
    if (options.type == 2) { //编辑跳转而来
      this.data.navBarTitleText = '编辑钥匙';
      wx.setNavigationBarTitle({
        title: this.data.navBarTitleText
      });
      keyModel = util.jsonParamParse(options.jsonParam);
    } else {
      const category = catArry[0];
      keyModel = {
        categoryID: category.id,
        categoryName: category.name,
        categoryColor: category.color,
        categorySort: category.sort
      }
    }
    this.setData({
      catArry: catArry,
      keyModel: keyModel
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: keyModel.categoryColor,
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})